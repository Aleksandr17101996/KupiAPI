# Используем официальный образ Python
FROM python:3.9-slim

# Устанавливаем рабочую директорию
WORKDIR /app

# Копируем файлы с зависимостями и тесты в образ
COPY requirements.txt .
COPY tests/ ./tests/

# Устанавливаем зависимости
RUN pip install -r requirements.txt

# Запускаем тесты
CMD ["pytest", "tests/"]